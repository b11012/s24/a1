
// S24 Activity:

// >> In the S19 folder, create an activity folder and an index.html and index.js file inside of it.

// >> Link the script.js file to the index.html file.

// >> Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

	// S O L U T I O N

			const getCube = 15 ** 3;
			console.log(getCube);
			// Result: 3375

// >> Using Template Literals, print out the value of the getCube variable with a message:
// 		 The cube of <num> is…
			
	// S O L U T I O N

			let numbEr = 15
			console.log(`The cube of ${numbEr} is ${getCube}`)

// >> Destructure the given array and print out a message with the full address using Template Literals.
	
// 		const address = ["258", "Washington Ave NW", "California", "90011"];

// 		message: I live at <details>
	
	// S O L U T I O N

		const address = ["258", "Washington Ave NW", "California", "90011"];

		const [houseNumber, county, state, zipCode] = address

		console.log(`I live at ${houseNumber} ${county} ${state} ${zipCode}`);

// >> Destructure the given object and print out a message with the details of the animal using Template Literals.
	

	//  S O L U T I O N

		const animal = {
			name: "Lolong",
			species: "saltwater crocodile",
			weight: "1075 kgs",
			measurement: "20 ft 3 in"
		};


		const {name, species, weight, measurement} = animal;

		console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`)

// 



// >> Loop through the given array of characters using forEach, an arrow function and using the implicit return statement to print out each character
		

	// S O L U T I O N

		const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']

		characters.forEach(hero => console.log(`${hero} are heroes`));

// >> Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

	
	// S O L U T I O N

		class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

	let myDog = new Dog();

	myDog.name = "Yoko";
	myDog.age = "6 months";
	myDog.breed = "American Bully";

	console.log(myDog);

// >> Create/instantiate a 2 new object from the class Dog and console log the object

	// S O L U T I O N
	
	const anotherDog = new Dog("Hachi", "10 yrs old", "Chihuahua" );
	console.log(anotherDog);